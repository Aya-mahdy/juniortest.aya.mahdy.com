<?php
namespace App\Controllers;
//use App\Models\Product;
use \Core\View;
use \App\Models\Products\Main;
use \App\Models\Products\Dvd;
use \App\Models\Products\Book;
use \App\Models\Products\Furniture;
//Poducts
class Products
{
    public function index()
    {
        $dvd_products = Dvd::getProduct();
        $book_products = Book::getProduct();
        $furniture_products = Furniture::getProduct();


        View::renderTemplate('Products/index.html',[

            'dvd_products' =>$dvd_products,
            'book_products' => $book_products,
            'furniture_products' => $furniture_products
        ]);
    }

    public function addProduct()
    {
        View::renderTemplate('Products/add.html');
    }

    public function SaveDvdProduct()
    {
        //recieving data 
        $data = [
            'sku' => $_POST['sku'],
            'name' => $_POST['name'],
            'price' => $_POST['price'],
            'size' => $_POST['size'],
            'productType' => $_POST['productType'],
        ];

        //create new instance
        $dvd = new Dvd();
        $dvd->ValidateProduct($data);
        
        //check errors
        if ( empty($dvd->errors) )
        {
            //create new product
            $dvd->setSku($data['sku']);
            $dvd->setName($data['name']);
            $dvd->setPrice($data['price']);
            $dvd->setSize($data['size']);
            $dvd->setProductType($data['productType']);
            $dvd->create(); 
        }



        View::renderTemplate('Products/add.html',[
        'errors' => $dvd->errors,
        'data' => $data
        ]); 
        
    }

    public function SaveBookProduct()
    {
        //recieving data 
        $data = [
            'sku' => $_POST['sku'],
            'name' => $_POST['name'],
            'price' => $_POST['price'],
            'weight' => $_POST['weight'],
            'productType' => $_POST['productType'],
        ];

        //create new instance
        $book = new Book();
        $book->ValidateProduct($data);
        
        //check errors
        if ( empty($book->errors) )
        {
            //create new product
            $book->setSku($data['sku']);
            $book->setName($data['name']);
            $book->setPrice($data['price']);
            $book->setWeight($data['weight']);
            $book->setProductType($data['productType']);
            $book->create(); 
        }
     
        //return to add page with errors
        View::renderTemplate('Products/add.html',[
        'errors' => $book->errors,
        'data' => $data
        ]); 
        
    }

    public function SaveFurnitureProduct()
    {
        //recieving data 
        $data = [
            'sku' => $_POST['sku'],
            'name' => $_POST['name'],
            'price' => $_POST['price'],
            'height' => $_POST['height'],
            'width' => $_POST['width'],
            'length' => $_POST['length'],
            'productType' => $_POST['productType'],
        ];

        //create new instance
        $furniture = new Furniture();
        $furniture->ValidateProduct($data);
        
        //check errors
        if ( empty($furniture->errors) )
        {
            //create new product
            $furniture->setSku($data['sku']);
            $furniture->setName($data['name']);
            $furniture->setPrice($data['price']);
            $furniture->setHeight($data['height']);
            $furniture->setWidth($data['width']);
            $furniture->setLength($data['length']);
            $furniture->setProductType($data['productType']);
            $furniture->create();   
        }
     



        View::renderTemplate('Products/add.html',[
        'errors' => $furniture->errors,
        'data' => $data
        ]); 
        
    }

    
    public function deleteItems()
    {
        if(isset($_POST['delete_records'])
        && isset($_POST['no'])
        && count($_POST['no']) > 0)
        {
            $no = $_POST['no'];
            Main::deleteAll($no);
            $products = Main::getAll();
            //Redirct to index page
            header('Location: index');
            
        }else
        {
            header('Location: index');
        }
      
    }
    

}