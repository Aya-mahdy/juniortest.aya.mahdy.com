<?php
namespace App;

//Application configration
class Config
{
    //Database host
    const DB_HOST = 'localhost';

    //Database name
    const DB_NAME = 'juniortest';

    //Database username
    const DB_USERNAME = 'root';

    //Database password
    const DB_PASSWORD = '';

    //Show or hide errors from screen
    const SHOW_ERRORS = false;
}