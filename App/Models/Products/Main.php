<?php
namespace App\Models\Products;

//Main product
abstract class Main extends \Core\Model
{
    protected $sku;
    protected $name;
    protected $price;
    protected $size;
    protected $weight;
    protected $height;
    protected $width;
    protected $length;
    protected $productType;
    public $errors;
 
    //Get all products in Associative array
    public static function getAll()
    {
             $conn = static::getDB();
             $sql = "SELECT * FROM products";
             $result = $conn->query($sql);
             //print_r($result);
             if($result->num_rows > 0)
             {
                 return $result->fetch_all(MYSQLI_ASSOC);
                 //print_r($result->fetch_all(MYSQLI_ASSOC));
             }else {
                 return [];
             }
                
    }

    abstract static function getProduct();
    
    //Validate product
    abstract function ValidateProduct($data);

    // SET Parametres
    public function setSku($sku)
    {
        $this->sku = $sku;
    }

    public function setName($name)
    {
        $this->name = $name;
    }

    public function setPrice($price)
    {
        $this->price = $price;
    }

    //check if sku exist or not 
    public static function checkSku($field)
      {
          $conn = static::getDB();
          $sql = "SELECT product_sku FROM products WHERE product_sku ='$field'";
          $result = $conn->query($sql);
          return (!$result->num_rows);
  
      }


    abstract function setSize($size);

    abstract function setWeight($weight);

    abstract function setHeight($height);

    abstract function setWidth($width);

    abstract function setLength($length);

    abstract function setProductType($productType);

    //create new row into db
    abstract function create();

    //save record to db
    public function save($query)
    {
          $conn = static::getDB();
          if (mysqli_query($conn, $query)) {
            //redirect to index
            header('Location: index');
        } else {
                echo "Error: " . $query . "<br>" . mysqli_error($conn);
        }
    }
    

    public static function deleteAll($no =[])
    {
        $conn = static::getDB();
        for($i=0;$i<count($no);$i++)
        {
            $row_no = $no[$i];
            $sql = "DELETE FROM products WHERE id='$row_no'";
            $result =  $conn->query($sql);
        }
    }

}