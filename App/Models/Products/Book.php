<?php 
namespace App\Models\Products;
use App\Models\Products\Main;
use \Core\Validator;
//Book 
class Book extends Main 
{
    public static function getProduct()
    {
        $conn = static::getDB();
             $sql = "SELECT * FROM products WHERE product_type = 2";
             $result = $conn->query($sql);
             //print_r($result);
             if($result->num_rows > 0)
             {
                 return $result->fetch_all(MYSQLI_ASSOC);
                 //print_r($result->fetch_all(MYSQLI_ASSOC));
             }else {
                return [];
            }
    }

    public function ValidateProduct($data)
    {
        //validate data
        $validator = new Validator();
        //sku (required - unique - max:20)
        if($validator->isEmpty($data['sku']))
        {
            $validator->errors['sku'] = 'Required field';
        }elseif ($validator->isUnique($data['sku'])){
            $validator->errors['sku'] = 'Must be Unique';
        }elseif ($validator->isNotMoreThan($data['sku'] ,20))
        {
            $validator->errors['sku'] = 'Invalid input';
        }

        //name (required - string - max:20)
        if($validator->isEmpty($data['name']))
        {
            $validator->errors['name'] = 'Required field';
        }elseif ($validator->isNumber($data['name'])){
            $validator->errors['name'] = 'Must be letters only';
        }elseif ($validator->isNotMoreThan($data['name'] ,20))
        {
            $validator->errors['name'] = 'Invalid input';
        }

        //price (required - Integer - max:11)
        if($validator->isEmpty($data['price']))
        {
            $validator->errors['price'] = 'Required field';
        }elseif (! $validator->isNumber($data['price'])){
            $validator->errors['price'] = 'Must be numbers only';
        }elseif ($validator->isNotMoreThan($data['price'] ,11))
        {
            $validator->errors['price'] = 'Invalid input';
        }

        //productType (required)
        if($validator->isEmpty($data['productType']))
        {
            $validator->errors['productType'] = 'Required field';
        }

        //validate weight (required - Integer - max:20)
        if($validator->isEmpty($data['weight']))
        {
            $validator->errors['weight'] = 'Required field';
        }elseif (! $validator->isNumber($data['weight'])){
            $validator->errors['weight'] = 'Must be numbers only';
        }elseif ($validator->isNotMoreThan($data['weight'] ,20))
        {
            $validator->errors['weight'] = 'Invalid input';
        }

        $this->errors = $validator->errors;
        

    }

    public function setSize($size)
    {
        $this->size = null;
    }

    public function setweight($weight)
    {
        $this->weight = $weight;
    }

    public function setheight($height)
    {
        $this->height = null;
    }

    public function setwidth($width)
    {
        $this->width = null;
    }

    public function setlength($length)
    {
        $this->length = null;
    }

    public function setProductType($productType)
    {
        $this->productType = 2;
    }



    public function create()
    {
        $conn = static::getDB();
        $query = "INSERT INTO products (product_sku, product_name, product_price, product_weight, product_Type)
        VALUES ('$this->sku','$this->name','$this->price','$this->weight',$this->productType) ";
        $this->save($query);
    }
    
}