<?php
//composer autoload
require '../vendor/autoload.php';
//Error and Exception handling
error_reporting(E_ALL);
set_error_handler('Core\Error::errorHandler');
set_exception_handler('Core\Error::exceptionHandler');

$router = new Core\Router();
$router->add('',['controller' => 'Products' ,'action'=>'index']);
$router->add('addProduct',['controller' => 'Products' ,'action'=>'addProduct']);
$router->add('{controller}/{action}');
$url = $_SERVER['QUERY_STRING'];
$router->dispatch($url);
//print_r($router->getParams());