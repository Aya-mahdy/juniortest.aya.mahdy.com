<?php
namespace Core;
use App\Config;

//base model
abstract class Model
{
    
    //Get db connection
    protected static function getDB()
    {
        static $conn = null;
        if($conn === null)
        {
            //create connection
            $conn = new \mysqli($servername = Config::DB_HOST, $username = Config::DB_USERNAME, $password = Config::DB_PASSWORD, $dbname = Config::DB_NAME);
            
            // Check connection
            if ($conn->connect_error) {
                die("Connection failed: " . $conn->connect_error);
            }
                //echo "Connected successfully";
        }

        return $conn;
    } 
}
