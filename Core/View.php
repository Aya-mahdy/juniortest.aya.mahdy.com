<?php
namespace Core;
//view
class View
{
    /* Render a view file
    string $view the view file
    array $args is Associative  array of data to display
    in view (optional)
    */

    protected static function render($view, $args = [])
    {
        extract($args , EXTR_SKIP);
        $file = '../App/Views/$view';
        if(is_readable($file))
        {
            require $file;
        } else {
            throw new \Exception("$file not found");
        }
    }

    //render a view template using twig
    public static function renderTemplate($template, $args = [])
    {
        static $twig = null;
        if ($twig === null)
        {
            $loader = new \Twig_Loader_Filesystem('../App/Views');
            $twig = new \Twig_Environment($loader);
        }
        echo $twig->render($template, $args);
    }
}