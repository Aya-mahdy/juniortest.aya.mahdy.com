<?php
namespace Core;
use App\Models\Products\Main;

class Validator 
{
    public $errors = [];
    

    //check if is empty or not 
    public function isEmpty($field)
    {
        return empty($field);
         
    }


    //check if there are numbers or not
    public function isNumber($field)
    {
        return is_numeric($field);
       
    }

    //check if is integer or not
    public function isInteger($field)
    {
        return (! is_numeric($field));

    }

    //check the length of inputs
    public function isNotMoreThan($field ,$maxLength)
    {
        $length = strlen($field);
        return ($length > $maxLength);
   
    }

    //check is unique or not
    public function isUnique($field)
    {
        return (!Main::checkSku($field));
     
    }


}

