<?php
namespace Core;
//Router

class Router
{
    //Associativr array for routes(routing table)
    protected $routes = [];
    //parameters from matched routes
    protected $params = [];

    /*Adding routes yo routing table
    sting $route route url
    array $params (controller,action,etc)
    */
    public function add($route, $params = [])
    {
           // Convert the route to a regular expression: escape forward slashes
           $route = preg_replace('/\//', '\\/', $route);

           // Convert variables e.g. {controller}
           $route = preg_replace('/\{([a-z]+)\}/', '(?P<\1>[a-z-]+)', $route);
   
           // Add start and end delimiters, and case insensitive flag
           $route = '/^' . $route . '$/i';

           $this->routes[$route] = $params;
    }

    //Get all routes from routing table
    public function getRoutes()
    {
        return $this->routes;
    }

    /* Match route to routing table setting $params 
    property if a route is found
    */

    protected function match($url)
    {
        foreach($this->routes as $route => $params)
        {
            if(preg_match($route, $url, $matches))
            {
                foreach($matches as $key => $match)
                {
                    if(is_string($key))
                    {
                        $params[$key] = $match;
                        
                    }
                }

                $this->params = $params;
                return true;
            }
        }
        return false;
    }

    //Get matched params
    public function getParams()
    {
        return $this->params;
    }

    //Dispatch route create the controller object and running method
    public function dispatch($url)
    {
        $url = $this->removeQueryStringVariables($url);

        if ($this->match($url)) {
            $controller = $this->params['controller'];
            $controller = $this->convertToStudlyCaps($controller);
            $controller = 'App\Controllers\\' . $controller;

            if (class_exists($controller)) {
                $controller_object = new $controller();

                $action = $this->params['action'];
                $action = $this->convertToCamelCase($action);

                if (is_callable([$controller_object, $action])) {
                    $controller_object->$action();

                } else {
                    throw new \Exception("Method $action (in controller $controller) not found");
                }
            } else {
                throw new \Exception("Controller class $controller not found");
            }
        } else {
            throw new \Exception('No route matched.', 404);
        }
    }


    //Convert the string with hyphens to StudlyCaps
    public function convertToStudlyCaps($string)
    {
        return str_replace(' ','',ucwords(str_replace('-',' ',$string)));
    }
    
    //Convert the string with hyphens to camelCase
    public function convertToCamelCase($string)
    {
        return lcfirst($this->convertToStudlyCaps($string));
    }

    //Remove the query string variables from the URL (if any)
    protected function removeQueryStringVariables($url)
    {
        if ($url != '') {
            $parts = explode('&', $url, 2);

            if (strpos($parts[0], '=') === false) {
                $url = $parts[0];
            } else {
                $url = '';
            }
        }

        return $url;
    }

}